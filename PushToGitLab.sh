#!/bin/bash
# This script automates the process of creating GitLab repositories, initializing a local repository, pushing changes to the repositories, and updating the README dynamically.

# Your GitLab IDs should be in a file named .gitlabToken (Not the safest way by the way...)
gitlabToken=$(cat ./.gitlabToken)
# Faire une requête GET vers l'API GitLab pour obtenir les informations de l'utilisateur
checkToken=$(curl --silent --header "PRIVATE-TOKEN: $gitlabToken" "https://gitlab.com/api/v4/user")
# Vérifier si la réponse contient "username"
if echo ${checkToken} | grep -q "username"; then
    echo "Le token est valide."
else
    echo "Le token est invalide ou a expiré."
    exit 1
fi

# Collect project & Group name
read -p "Quel est le nom du projet GitLab -> " projectName
read -p "Quel est le nom du groupe GitLab -> " groupName

# Find Group ID
groupId=$(curl --silent --header "PRIVATE-TOKEN: ${gitlabToken}" "https://gitlab.com/api/v4/groups?search=lacapsule-batch6" | jq '.[0].id')

# What's the date and time
currentDate=$(date '+%d-%m-%Y')
currentTime=$(date '+%H:%M:%S')

# Initialize local repository
mkdir -p ${projectName}
cd ${projectName}
git init --initial-branch=main

# Create README.md
cat << EOF > ReadMe.MD
# ${projectName}

This project is for educationnal purpose only - Made with ❤️ and lots of coffee!
 
Hey! If you're reading this, know that this code is COMPLETELY free of rights.
You can copy it, modify it, distribute it, or even sing it a lullaby but please don't delete this README file.
 
However, a little shoutout or thank you would be appreciated if this code helps you out.
Any financial contribution would also encourage me to maintain the code
You may send any crypto to 0x432428923B4F06c10E8A5a98044D09A2DFCa5Ee5
But nothing is mandatory...
## Licence

MIT License  ![MIT License](https://img.shields.io/badge/License-MIT-green.svg)

Copyright (c) 2024 Frederic LANEQUE 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
## Authors

- [@fredlaneque](https://www.github.com/fredlaneque)

## Roadmap

+ Your suggestions are welcome...
## Running Tests

Supposed to explain how to run it
## This project started

Created ${currentDate} at ${currentTime} tested using Python 3.12"
EOF

# Create .gitlab-ci.yml
cat << EOF > .gitlab-ci.yml
image: python:3.12

stages:
  - lint

flake8:
  stage: lint
  script:
    - pip install --upgrade pip
    - pip install flake8
    - flake8 .
EOF

# Create .gitignore
cat << EOF > .gitignore
# See https://help.github.com/articles/ignoring-files/ for more about ignoring files.

# dependencies
/node_modules
/.pnp
.pnp.js

# testing
/coverage

# production
/build

# misc
.DS_Store
.env.local
.env.development.local
.env.test.local
.env.production.local

npm-debug.log*
yarn-debug.log*
yarn-error.log*
EOF

# Create repository
curl --silent --header "PRIVATE-TOKEN: ${gitlabToken}" --data "name=${projectName}" https://gitlab.com/api/v4/projects?namespace_id=${groupId} > /dev/null

# Add repository as remote
git remote add ${projectName} "https://gitlab.com/${groupName}/${projectName}.git"

# Prompt for creating additional feature branches (up to 5)
for ((i=1; i<=5; i++)); do
    read -p "Voulez vous créer une branche additionnelle ? (o/N): " choice
    if [ "$choice" == "o" ]; then
        read -p "$i. Quel est le nom de la branche à créer -> " branchName
        read -p "$i. Commentaire pour le commit -> " branchComment
        git checkout -b ${branchName}
        git add .
        git commit -m "${branchComment}"
        git push ${projectName} ${branchName}
    else
        break
    fi
done

# Switch to main branch
git checkout main

# Commit changes to README and push to both repositories
read -p "$i. Commentaire pour le commit (main) (CTRL+C pour skip) -> " mainComment
git add .
git commit -m "${projectName} ${mainComment}"
git push -u ${projectName} main

echo "Votre projet ${projectName} a bien été créé dans le groupe ${groupName}"